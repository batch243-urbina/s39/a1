// Require the jsonwebtoken module and then contain it in jwt variable
const { response } = require("express");
const jwt = require("jsonwebtoken");

// Used in algorithm for encrypting our data which makes it difficult to decode the information without defined secret key
const secret = "CourseBookingAPI";

// [SECTION] JSON Web Token

// Token creation
/*
	Analogy:
		Pack the gift provided with a lock, which can only be opened using the secret code as the key.
*/

module.exports.createAccessToken = (user) => {
  // Payload of the JWT
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };

  // Generate a JSON web token using the JWT's sign method
  //Syntax
  // jwt.sign(payload, secretOrPrivateKey, [callback function])

  return jwt.sign(data, secret, {});
};

// TOKEN VERIFICATION
// Receive the gift and open the lock to verify if the sender is legitimate and the gift was not tampered

// Validate the 'token' using verify method, to decrypt the token using the secret code
// jwt.verify(token, secret, [callback function])
module.exports.verify = (request, response, next) => {
  let token = request.headers.authorization;
  console.log(token);

  if (token !== undefined) {
    token = token.slice(7, token.length);
    return jwt.verify(token, secret, (error, data) => {
      if (error) {
        return response.send(`Invalid token`);
      } else {
        next();
      }
    });
  } else {
    return response.send(`Authentication failed! No token provided`);
  }
};

// TOKEN DECRYPTION

module.exports.decode = (token) => {
  if (token === undefined) {
    return null;
  } else {
    token = token.slice(7, token.length);

    return jwt.verify(token, secret, (error, data) => {
      if (error) {
        return null;
      } else {
        // decode method is used to obtain the information from the jwt
        // jwt.decode(token, [options])
        // will return an object with the access to the payload property
        return jwt.decode(token, { complete: true }).payload;
      }
    });
  }
};
