const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth");

// Create new course
router.post("/", auth.verify, courseController.addCourse);

// Get all active courses
router.get("/allActiveCourses", courseController.getAllActive);

// Retrieve all courses
router.get("/allCourses", auth.verify, courseController.getAllCourses);

// Get Specific course
router.get("/:courseId", courseController.getCourse);

// Update specific course
router.put("/update/:courseId", auth.verify, courseController.updateCourse);

// Archive specific course
router.patch("/archive/:courseId", auth.verify, courseController.archiveCourse);

module.exports = router;
